package com.br.sapestoque;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.br.sapestoque.databinding.ActivityDetailBinding;
import com.br.sapestoque.model.Produto;

public class DetailActivity extends AppCompatActivity {

    private ActivityDetailBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail);

        mBinding.buttonConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Produto produto = (Produto) getIntent().getExtras().get("PRODUTO");
        mBinding.nome.setText(produto.getNome());
        mBinding.descricao.setText(produto.getDescricao());
        mBinding.quantidade.setText(produto.getUnidade());
        mBinding.precoCusto.setText(produto.getPrecoCusto());
        mBinding.precoTotal.setText(produto.getPrecoTotal());
    }
}

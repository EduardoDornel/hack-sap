package com.br.sapestoque;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Toast;

import com.br.sapestoque.databinding.ActivityMainBinding;
import com.br.sapestoque.model.Produto;
import com.br.sapestoque.req.ReqGoogle;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mBinding;

    private static final String TAG = "MainActivity";
    private static final int requestPermissionID = 101;
    private static final int CAMERA_AND_STORAGE_RC = 12;


    private Uri mPhotoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        configChart();

        mBinding.buttonTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });
    }

    private void configChart() {
        ArrayList<PieEntry> yvalues = new ArrayList<>();
        yvalues.add(new PieEntry(8f, "Blusa"));
        yvalues.add(new PieEntry(15f, "Calça"));
        yvalues.add(new PieEntry(12f, "Vestido"));
        yvalues.add(new PieEntry(23f, "Pijama"));
        yvalues.add(new PieEntry(17f, "Biquini"));

        PieDataSet dataSet = new PieDataSet(yvalues, "Gráfico do estoque");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData data = new PieData(dataSet);
        data.setValueTextSize(14f);
        mBinding.pieChart.setData(data);
        mBinding.pieChart.animateXY(1000, 1000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                requestCode == CAMERA_AND_STORAGE_RC) {
            takePicture();
        }
    }

    public boolean requestCameraPermission(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[] {
                            Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    CAMERA_AND_STORAGE_RC);
            return false;
        }
        return true;
    }

    public void takePicture() {
        if (!requestCameraPermission(this)) {
            return;
        }

        mPhotoUri = getOutputMediaFile();
        Intent pickImageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);

        Intent chooserIntent = Intent.createChooser(pickImageIntent, "Escolhe da câmera ou galeria");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takePhotoIntent});

        startActivityForResult(chooserIntent, requestPermissionID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestPermissionID && resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                mPhotoUri = data.getData();
            }

            CropImage.activity(mPhotoUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                try {
                    final Uri resultUri = result.getUri();

                    String filePath = getRealPathFromURI(resultUri.toString());

                    Bitmap mPhotoBitmap = BitmapFactory.decodeFile(filePath);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    mPhotoBitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos);

                    byte[] byteArrayImage = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

                    final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Carregando", true);

                    ReqGoogle.obterDados(encodedImage, new ReqGoogle.ListenerRequestImage() {
                        @Override
                        public void onSuccess(Produto produto) {
                            progressDialog.cancel();

                            Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                            intent.putExtra("PRODUTO", produto);
                            startActivity(intent);
                        }

                        @Override
                        public void onError() {
                            progressDialog.cancel();
                            Toast.makeText(MainActivity.this, "Erro na leitura, tente novamente", Toast.LENGTH_LONG).show();
                        }
                    });

                    baos.close();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Não foi possível carregar a imagem.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            String indexString = cursor.getString(index);
            cursor.close();
            return indexString;
        }
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "TagLivros");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    public Uri getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "TagLivros");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File file = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + UUID.randomUUID().toString() + ".jpg");

        return Uri.fromFile(file);
    }
}

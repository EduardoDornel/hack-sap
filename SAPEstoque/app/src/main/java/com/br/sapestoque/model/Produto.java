package com.br.sapestoque.model;

import java.io.Serializable;

public class Produto implements Serializable {

    private String nome;
    private String descricao;
    private String unidade;
    private String precoCusto;
    private String precoTotal;

    public Produto(String nome, String descricao, String unidade, String precoCusto, String precoTotal) {
        this.nome = nome;
        this.descricao = descricao;

        this.unidade = unidade;
        this.precoCusto = precoCusto;
        this.precoTotal = precoTotal;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getPrecoCusto() {
        return precoCusto;
    }

    public void setPrecoCusto(String precoCusto) {
        this.precoCusto = precoCusto;
    }

    public String getPrecoTotal() {
        return precoTotal;
    }

    public void setPrecoTotal(String precoTotal) {
        this.precoTotal = precoTotal;
    }
}

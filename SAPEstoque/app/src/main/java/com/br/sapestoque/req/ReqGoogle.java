package com.br.sapestoque.req;

import com.br.sapestoque.model.Produto;

import net.trippedout.cloudvisionlib.CloudVisionApi;
import net.trippedout.cloudvisionlib.CloudVisionService;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReqGoogle {

    public interface ListenerRequestImage {
        void onSuccess(Produto produto);
        void onError();
    }

    public static void obterDados(String encodedData, final ListenerRequestImage listener) {
        CloudVisionService mCloudVisionService = CloudVisionApi.getCloudVisionService();

        Call<CloudVisionApi.VisionResponse> call = mCloudVisionService.getAnnotations(
                "AIzaSyCn2iFK7AlJoAdi80GUS2xK0DXAD1C26kg", // you get this key from the getting-started steps above
                CloudVisionApi.getTestRequestAllFeatures(encodedData) // uses all possible features with default result number
        );

        call.enqueue(new Callback<CloudVisionApi.VisionResponse>() {
            @Override
            public void onResponse(Call<CloudVisionApi.VisionResponse> call, Response<CloudVisionApi.VisionResponse> response) {
                CloudVisionApi.TextResponse textDetection =(CloudVisionApi.TextResponse)response.body().getResponseByType(CloudVisionApi.FEATURE_TYPE_TEXT_DETECTION);

                if (textDetection == null) {
                    listener.onError();
                    return;
                }

                String[] infosNecessarias = textDetection.textAnnotations.get(0).description.split("\n");

                if (infosNecessarias.length >= 5) {
                    Produto produto = new Produto(infosNecessarias[0],
                            infosNecessarias[1],
                            infosNecessarias[2],
                            infosNecessarias[3],
                            infosNecessarias[4]);

                    listener.onSuccess(produto);
                } else {
                    listener.onError();
                }
            }

            @Override
            public void onFailure(Call<CloudVisionApi.VisionResponse> call, Throwable t) {
                t.printStackTrace();
                listener.onError();
            }
        });
    }

}
